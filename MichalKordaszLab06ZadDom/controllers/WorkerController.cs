﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MichalKordaszLab06ZadDom.api.models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MichalKordaszLab06ZadDom.api.controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkerController : ControllerBase
    {
        private static Dictionary<string, Worker> _worker;

        static WorkerController()
        {
            _worker = new Dictionary<string, Worker>();
        }

        /// <summary>
        /// Pobranie wszystkich pracowników
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var allWorkers = _worker.Select(x => x.Value);

            return Ok(allWorkers);
        }


    }
}