﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MichalKordaszLab06ZadDom.api.models;
using Microsoft.AspNetCore.Mvc;

namespace MichalKordaszLab06ZadDom.api.controllers
{
    [Route("api/students")]
    [ApiController]
    public class TramController : Controller
    {
        private static Dictionary<string, Tram> _tram;

        static TramController()
        {
            _tram = new Dictionary<string, Tram>();
        }

        /// <summary>
        /// Pobranie wszystkich danych.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var allTrams = _tram.Select(x => x.Value);

            return Ok(allTrams);
        }

        /// <summary>
        /// Dodanie danych.
        /// </summary>
        /// <param name="tram"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] Tram tram)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var id = Guid.NewGuid().ToString();

            tram.carNo = Int32.Parse(id);

            _tram.Add(id, tram);

            return NoContent();
        }

        /// <summary>
        /// Pobranie po numerze pojazdu.
        /// </summary>
        /// <param name="carNo"></param>
        /// <returns></returns>
        [HttpGet("{carNo}")]
        public IActionResult Get(string carNo)
        {
            var searchedResult = _tram.TryGetValue(carNo, out Tram tram);
            if (!searchedResult)
                return NotFound();
            return Ok(tram);
        }

        /// <summary>
        /// Edycja obiektu o numerze pojazdu.
        /// </summary>
        /// <param name="carNo"></param>
        /// <param name="tram"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put(string carNo, [FromBody] Tram tram)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var searchedResult = _tram.TryGetValue(carNo, out Tram existTram);
            if (!searchedResult)
                return NotFound();

            existTram.carNo = tram.carNo;
            existTram.modelName = tram.modelName;
            existTram.yearProduction = tram.yearProduction;
            existTram.inUse = tram.inUse;


            return NoContent();
        }

    }
}