﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MichalKordaszLab06ZadDom.api.models
{
    public class Tram
    {
        [Required]
        public int carNo { get; set; }
        [Range(1950, 2018)]
        public int yearProduction { get; set; }
        [StringLength(20)]
        public string modelName { get; set; }
        [Required]
        public bool inUse { get; set; }
    }
}
