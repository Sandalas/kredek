﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MichalKordaszLab06ZadDom.api.models
{
    public class Worker
    {
        [Required]
        public string name { get; set; }
        [EmailAddress]
        public string email { get; set; }
        [Required]
        public string address { get; set; }
    }
}
